docker-service role
=========

Role to deploy docker services on a swarm cluster. This role will use the files on ../compose/path/to/service to start a docker service. On that directory there must be the docker-compose.yml file, a .env file and any other configuration file needed to launch the service.

Role Variables
--------------

---
    service:
      project_name
      hostname
      swarm_mode: true
      state:
      type:
    fs_volumes:
    registry_auth: "{{ site.registry_auth | default('false') }}"
    service_configs:
    env:
    compose_file_name: "docker-compose.yml"
    local_compose_file_name: "docker-compose.{{ env }}.yml"
    fix_permissions: false


Example Playbook
----------------

    - name: Deploy service
      include_role:
        name: docker-service
      vars:
        service:
          hostname: swarmprom
          project_name: infra-swarmprom
          type: "infra"
          state: "{{ service_state }}"
          fs_volumes:
            - /var/compose/infra/portainer/volumes/data

License
-------

GPLv2 or later

Author Information
------------------

* Juan Luis Baptiste < juan _at_ juanbaptiste _dot_ tech >
* https://www.juanbaptiste.tech
